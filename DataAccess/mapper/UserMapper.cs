﻿using AutoMapper;
using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.model;

namespace VideoGamesAPI.DataAccess.mapper;
public class UserMapper : Profile
{
    public UserMapper()
    {
        CreateMap<UserDto, UserEntity>().ReverseMap();
        CreateMap<LoginDto, UserEntity>().ReverseMap();
        CreateMap<RegisterDto, UserEntity>().ReverseMap();
    }
}