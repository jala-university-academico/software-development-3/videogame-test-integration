﻿using VideoGamesAPI.BusinessLogic.dto;

namespace VideoGamesAPI.DataAccess.repositorty;

/// <summary>
/// Interfaz que representa el repositorio de usuarios.
/// </summary>
public interface IUserRepository
{
    IEnumerable<UserDto> GetUsers();
    UserDto AddUser(RegisterDto user);
    UserDto GetUserByUsernameAndPassword(string username, string password);
    UserDto GetUserByUsername(string username);
}