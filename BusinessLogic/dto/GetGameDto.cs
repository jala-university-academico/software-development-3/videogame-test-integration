﻿namespace VideoGamesAPI.BusinessLogic.dto;
    public class GetGameDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Platform { get; set; }
        public decimal Price { get; set; }
    }

