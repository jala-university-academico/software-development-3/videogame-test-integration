﻿using System.ComponentModel.DataAnnotations;

namespace VideoGamesAPI.BusinessLogic.dto;

public class GameDto
{
    [Required(ErrorMessage = "El nombre del juego es obligatorio.")]
    [StringLength(50, ErrorMessage = "El nombre del juego no puede tener más de 50 caracteres.")]
    public string Name { get; set; }

    [Required(ErrorMessage = "La plataforma del juego es obligatoria.")]
    [RegularExpression("^(Xbox|Play Station)$", ErrorMessage = "La plataforma solo puede ser Xbox o Play Station.")]
    public string Platform { get; set; }

    [Required(ErrorMessage = "El precio del juego es obligatorio.")]
    [Range(0.01, double.MaxValue, ErrorMessage = "El precio del juego debe ser mayor que cero.")]
    public decimal Price { get; set; }

}