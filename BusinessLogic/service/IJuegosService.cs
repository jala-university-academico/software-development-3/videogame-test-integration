﻿using VideoGamesAPI.BusinessLogic.dto;

namespace VideoGamesAPI.BusinessLogic;

/// <summary>
/// Interfaz que representa la capa de servicio para administrar juegos.
/// </summary>
public interface IGamesService
{
    IEnumerable<GetGameDto> GetGames();
    GetGameDto GetGame(Guid id);
    GetGameDto CreateGame(GameDto game);
    GetGameDto UpdateGame(Guid id, GameDto game);
    void DeleteGame(Guid id);
}