﻿using VideoGamesAPI.BusinessLogic.dto;

namespace VideoGamesAPI.BusinessLogic.service;

/// <summary>
/// Interfaz que representa el servicio de usuarios.
/// </summary>
public interface IUserService
{
    IEnumerable<UserDto> GetUsers();
}
