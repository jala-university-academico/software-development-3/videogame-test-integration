﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace VideoGamesAPI.BusinessLogic.service;

/// <summary>
/// Clase utilitaria para la generación de tokens JWT.
/// </summary>
public class JwtUtil
{
    private readonly string _issuer;
    private readonly string _secretKey;
    private readonly string _roleClaimType;

    public JwtUtil(string issuer, string secretKey, string roleClaimType)
    {
        _issuer = issuer;
        _secretKey = secretKey;
        _roleClaimType = roleClaimType;
    }

    /// <summary>
    /// Método para crear un token JWT.
    /// </summary>
    /// <param name="username">El nombre de usuario asociado al token.</param>
    /// <param name="role">El rol del usuario asociado al token.</param>
    /// <returns>El token JWT generado.</returns>
    public string Create(string username, string role)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_secretKey);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                    new Claim(ClaimTypes.Name, username),
                    new Claim(_roleClaimType, role)
                }),
            Expires = DateTime.UtcNow.Add(TimeSpan.FromDays(15)),
            Issuer = _issuer,
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }
}